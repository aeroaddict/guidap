<?php

namespace GuidapBundle\Listener;

use Symfony\Component\HttpFoundation\Session\Session;
use GuidapBundle\Event\ChoiceEvent;
use Symfony\Component\HttpFoundation\JsonResponse;

class ChoiceListener{

protected $result;
  public function processChoice(ChoiceEvent $event){
    $session = new Session();
    $mysteryNumber = $session->get('mystery_number');
    $this->choice = $event->getNumber();
    if ($session->get('result')=="Félicitation vous avez trouvé le nombre magique!!!"){
      $session->clear();
    }
    if ($this->choice > $mysteryNumber ){
      if (!empty($session->get('result'))){
        $session->set('result',$session->get('result').' '.$this->choice.' (+)');
      } else {
        $session->set('result',$this->choice.' (+)');
      }
    } else if ($this->choice < $mysteryNumber ){
      if (!empty($session->get('result'))){

        $session->set('result',$session->get('result').' '.$this->choice.' (-)');
      } else {

        $session->set('result',$this->choice.' (-)');
      }
    } else {
        $session->clear();
        $session->set('result',"Félicitation vous avez trouvé le nombre mystère!!!");
    }
    $this->result=$session->get('result');

  }
  public function getResult(){
    return $this->result;
  }
}
