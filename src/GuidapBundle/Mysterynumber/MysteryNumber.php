<?php

namespace GuidapBundle\Mysterynumber;

use Symfony\Component\HttpFoundation\Session\Session;

class MysteryNumber{
  /**
  *
  * Sauvegarde en session le nombre mysterieux
  *
  */
  public function createNumber(){
    $session = new Session();
    if (empty($session->get('mystery_number'))){
      $session->set('mystery_number', rand(0,100));
    }
  }
}
