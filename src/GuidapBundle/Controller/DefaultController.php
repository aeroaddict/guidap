<?php

namespace GuidapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('GuidapBundle:Default:index.html.twig');
    }
}
