<?php

namespace GuidapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use GuidapBundle\Listener\ChoiceListener;
use GuidapBundle\GuidapChoiceEvent;
use GuidapBundle\Event\ChoiceEvent;


class ApiController extends Controller
{

  public function resultatAction(Request $request){
    if ($request->isXmlHttpRequest()){
      $referer=$this->container->get('guidap.referer');
      if (!$referer->testReferer($request)){
        return new JsonResponse(array('result'=>'You shall not pass !'));
      }
      $session = new Session();

      $mysteryNumber= $this->container->get('guidap.mysterynumber');
      $mysteryNumber->createNumber();
      $event = new ChoiceEvent();
      $event->setNumber($request->get('number'));
        $listener= $this->container->get('guidap.processchoice');
        $listener->processChoice($event);
    return new JsonResponse(array('result'=>$listener->getResult()));
    }
      return new JsonResponse(array('result'=>'You shall not pass'));
  }
}
