<?php

namespace GuidapBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ChoiceEvent extends Event{
  protected $number = null;

  public function setNumber($number){
    $this->number = $number;
  }

  public function getNumber(){
    return $this->number;
  }
}
