<?php

namespace GuidapBundle\Referer;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;

class BlocageReferer{
  private $router;


  public function __construct($router){
    $this->router=$router;

  }
/**
*
* Blocage si l'appel n'est pas fait depuis le formulaire de la page d'acceuil
*
*/
  public function testReferer(Request $request){
    if ($this->router->generate('guidap_homepage',array(), UrlGeneratorInterface::ABSOLUTE_URL)!=$request->server->get('HTTP_REFERER')){
      return false;
    }
    return true;
  }
}
